FROM alpine:latest
LABEL maintainer Jan Szumiec <jan.szumiec@gmail.com>
RUN apk add --no-cache curl bzip2 tar gzip
WORKDIR /
RUN \ 
  wget https://ftp.fau.de/kiwix/bin/0.10/kiwix-0.10-linux-x86_64.tar.bz2 && \
  tar -xvjf kiwix-0.10-linux-x86_64.tar.bz2 && \
  mv kiwix-0.10-x86_84 kiwix
RUN \ 
  wget -O kiwix-data.tar.gz https://www.dropbox.com/s/4ufjaiezuzj245y/kiwix-data.tar.gz?dl=1 && \
  tar -xvzf kiwix-data.tar.gz
RUN \
  wget -O /tmp/ki https://gitlab.com/vms20591/kiwixdocker/raw/master/ki && \
  sh /tmp/ki
EXPOSE 8080
ENTRYPOINT ["/kiwix/bin/kiwix-serve", "--verbose","--port", "8080", "--library", "/kiwix-data/library.xml"]
